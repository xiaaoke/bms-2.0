# BMS 2.0

#### Description
Background management system based on node Express & mongodb & bootstrap & MD5

#### Software Architecture
node express & mongodb & bootstrap & md5

#### Installation

1.  Direct download
2.  Node runs mongodbdata.js to add data
3.  Just run index.js on node

#### Instructions

1.  First run mongodbdata. JS to add data to the database
2.  In mongodb database_ Create a user in DB_ Table set
3.  run index.js on node
4.  First create a user name and password for login

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
