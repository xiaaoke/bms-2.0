# BMS 2.0

#### 介绍
使用node express & mongodb & bootstrap & md5加密 编写的后台管理系统

#### 软件架构
node express & mongodb & bootstrap & md5加密 


#### 安装教程

1.  直接下载
2.  node 运行mongodbData.js加数据
3.  node运行index.js运行即可

#### 使用说明

1.  先运行mongodbData.js为数据库添加数据
2.  在mongodb数据库中user_db中创建一个user_table集合
3.  node运行index.js运行
4.  先创建一个用户名和密码在进行登录

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
