// 引入工具文件
var db = require("./app_data.js");

// 数据插入到mongodb数据库格式
db.mongodb({
    url : 'mongodb://localhost:27017', // 服务地址
    db : 'user_db',         // 创建的数据库的名称
    table: 'user_table',    // 创建的集合的名称
});