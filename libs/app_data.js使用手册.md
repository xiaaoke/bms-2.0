// 数据使用在两种数据库种的格式

引入工具文件
var db = require("./app_data.js");

数据插入到mysql数据库格式
db.mysql({
    host : 'localhost',  // 连接地址
    user : 'root',       // 连接用户名
    password : 'root',    // 连接密码 
    db : 'user_db',        // 创建的数据库名称
    table: 'user_table',    // 创建的表的名称
});

数据插入到mongodb数据库格式
db.mongodb({
    url : 'mongodb://localhost:27017', // 服务地址
    db : 'user_db',         // 创建的数据库的名称
    table: 'user_table',    // 创建的集合的名称
});

使用其中一种方式即可