// 导入mysql模块
const mysql = require('mysql');
// 导入mongodb模块   并且创建一个客户端对象
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require("mongodb").ObjectId;

// 数据库地址
const url = 'mongodb://127.0.0.1:27017';

// 声明数据库名
const dbName = 'user_db';

// 连接MySQL数据库
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '123',
  database : 'user_db'
});

module.exports = {
    // MySQL增删改查
    mysql(sql, callback){
        connection.query(sql, function (error, results, fields) {
            if (error) throw error;
            callback(results);
        });
    },

    // MongoDB查询数据
    find(collectionName, query, callback) {
        // 调用连接数据库的方法
        MongoClient.connect(url, { useUnifiedTopology: true }, function (err, client) {
            const db = client.db(dbName);

            // 选择使用的集合
            const collection = db.collection(collectionName);

            // 查询数据
            collection.find(query).toArray(function (err, docs) {
                callback(docs);

                // 关闭数据库连接
                client.close();
            });
        });
    },

    // MongoDB新增数据
    insertOne(collectionName, insertData, callback) {
        // 调用连接数据库的方法
        MongoClient.connect(url, { useUnifiedTopology: true }, function (err, client) {
            const db = client.db(dbName);

            // 选择使用的集合
            const collection = db.collection(collectionName);

            // 插入单条数据
            collection.insertOne(insertData, function (err, result) {
                callback(result);

                // 关闭数据库连接
                client.close();
            });
        });
    },

    // MongoDB删除数据
    deleteOne(collectionName, query, callback) {
        // 调用连接数据库的方法
        MongoClient.connect(url, { useUnifiedTopology: true },  function (err, client) {
            const db = client.db(dbName);

            // 选择使用的集合
            const collection = db.collection(collectionName);

            // 删除数据
            collection.deleteOne(query, (err, result) => {
                callback(result);

                // 关闭数据库
                client.close();
            });
        });
    },

    // MongoDB删除数据
    deleteMeny(collectionName, query, callback) {
        // 调用连接数据库的方法
        MongoClient.connect(url, { useUnifiedTopology: true },  function (err, client) {
            const db = client.db(dbName);

            // 选择使用的集合
            const collection = db.collection(collectionName);

            // 删除数据
            collection.deleteMeny(query, (err, result) => {
                callback(result);

                // 关闭数据库
                client.close();
            });
        });
    },

    // MongoDB修改数据
    updateOne(collectionName,  query, updateData, callback ) {
        // 调用连接数据库的方法
        MongoClient.connect(url, { useUnifiedTopology: true }, function (err, client) {
            const db = client.db(dbName);

            // 选择使用的集合
            const collection = db.collection(collectionName);

            // 修改数据
            collection.updateOne(query, { $set: updateData }, (err, result) => {
                callback(result);

                // 关闭数据库
                client.close();
            });
        });
    },

    // 暴露MongoDB ObjectId方法
    ObjectId
}
