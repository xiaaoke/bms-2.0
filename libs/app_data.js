(function () {
    // 数据
    const Data = [
        {
            userId: 202000001,
            userName: '赵壹',
            userSex: '男',
            userAge: 25,
            userBirth: '1995/12/01',
            userPhone: 13544231201,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000002,
            userName: '赵贰',
            userSex: '女',
            userAge: 25,
            userBirth: '1995/12/02',
            userPhone: 13544231202,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000003,
            userName: '赵叁',
            userSex: '男',
            userAge: 25,
            userBirth: '1995/12/03',
            userPhone: 13544231203,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000004,
            userName: '赵肆',
            userSex: '女',
            userAge: 30,
            userBirth: '1990/12/04',
            userPhone: 13544231204,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000005,
            userName: '赵伍',
            userSex: '男',
            userAge: 30,
            userBirth: '1990/12/05',
            userPhone: 13544231205,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000006,
            userName: '赵陆',
            userSex: '女',
            userAge: 34,
            userBirth: '1996/12/06',
            userPhone: 13544231206,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000007,
            userName: '赵柒',
            userSex: '女',
            userAge: 34,
            userBirth: '1996/12/07',
            userPhone: 13544231207,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000008,
            userName: '赵捌',
            userSex: '男',
            userAge: 24,
            userBirth: '1996/12/08',
            userPhone: 13544231208,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000009,
            userName: '赵玖',
            userSex: '女',
            userAge: 35,
            userBirth: '1985/12/09',
            userPhone: 13544231209,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000010,
            userName: '赵拾',
            userSex: '男',
            userAge: 35,
            userBirth: '1985/12/10',
            userPhone: 13544231210,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000011,
            userName: '钱壹',
            userSex: '男',
            userAge: 35,
            userBirth: '1985/12/11',
            userPhone: 13544231211,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000012,
            userName: '钱贰',
            userSex: '女',
            userAge: 25,
            userBirth: '1995/12/12',
            userPhone: 13544231212,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000013,
            userName: '钱叁',
            userSex: '男',
            userAge: 25,
            userBirth: '1995/12/13',
            userPhone: 13544231213,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000014,
            userName: '钱肆',
            userSex: '女',
            userAge: 25,
            userBirth: '1995/12/14',
            userPhone: 13544231214,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000015,
            userName: '钱伍',
            userSex: '女',
            userAge: 20,
            userBirth: '2000/12/15',
            userPhone: 13544231215,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000016,
            userName: '钱陆',
            userSex: '男',
            userAge: 20,
            userBirth: '2000/12/16',
            userPhone: 13544231216,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000017,
            userName: '钱柒',
            userSex: '女',
            userAge: 20,
            userBirth: '2000/12/17',
            userPhone: 13544231217,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000018,
            userName: '钱捌',
            userSex: '男',
            userAge: 40,
            userBirth: '1980/12/18',
            userPhone: 13544231218,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000019,
            userName: '钱玖',
            userSex: '女',
            userAge: 40,
            userBirth: '1980/12/19',
            userPhone: 13544231219,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000020,
            userName: '钱拾',
            userSex: '男',
            userAge: 40,
            userBirth: '1980/12/20',
            userPhone: 13544231220,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000021,
            userName: '孙壹',
            userSex: '男',
            userAge: 30,
            userBirth: '1990/12/21',
            userPhone: 13544231221,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000022,
            userName: '孙贰',
            userSex: '女',
            userAge: 33,
            userBirth: '1987/12/22',
            userPhone: 13544231222,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000023,
            userName: '孙叁',
            userSex: '男',
            userAge: 24,
            userBirth: '1996/12/23',
            userPhone: 13544231223,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000024,
            userName: '孙肆',
            userSex: '女',
            userAge: 23,
            userBirth: '1997/12/24',
            userPhone: 13544231224,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000025,
            userName: '孙伍',
            userSex: '男',
            userAge: 33,
            userBirth: '1987/12/25',
            userPhone: 13544231226,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000026,
            userName: '孙陆',
            userSex: '男',
            userAge: 34,
            userBirth: '1986/12/26',
            userPhone: 13544231226,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000027,
            userName: '孙柒',
            userSex: '女',
            userAge: 34,
            userBirth: '1986/12/27',
            userPhone: 13544231227,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000028,
            userName: '孙捌',
            userSex: '女',
            userAge: 33,
            userBirth: '1987/12/28',
            userPhone: 13544231228,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000029,
            userName: '孙玖',
            userSex: '男',
            userAge: 28,
            userBirth: '1992/12/29',
            userPhone: 13544231229,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000030,
            userName: '孙拾',
            userSex: '女',
            userAge: 29,
            userBirth: '1991/12/30',
            userPhone: 13544231230,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000031,
            userName: '李壹',
            userSex: '女',
            userAge: 24,
            userBirth: '1996/12/31',
            userPhone: 13544231231,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000032,
            userName: '李贰',
            userSex: '男',
            userAge: 25,
            userBirth: '1995/12/22',
            userPhone: 13544231232,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000033,
            userName: '李四',
            userSex: '男',
            userAge: 34,
            userBirth: '1986/12/22',
            userPhone: 13544231233,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000034,
            userName: '李肆',
            userSex: '男',
            userAge: 33,
            userBirth: '1987/12/22',
            userPhone: 13544231234,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000035,
            userName: '李伍',
            userSex: '女',
            userAge: 30,
            userBirth: '1990/12/22',
            userPhone: 13544231235,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000036,
            userName: '李陆',
            userSex: '男',
            userAge: 25,
            userBirth: '1995/12/22',
            userPhone: 13544231236,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000037,
            userName: '李柒',
            userSex: '女',
            userAge: 20,
            userBirth: '2000/12/22',
            userPhone: 13544231237,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000038,
            userName: '李捌',
            userSex: '男',
            userAge: 50,
            userBirth: '1970/12/22',
            userPhone: 13544231238,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000039,
            userName: '李玖',
            userSex: '女',
            userAge: 26,
            userBirth: '1994/12/22',
            userPhone: 13544231239,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000040,
            userName: '李拾',
            userSex: '男',
            userAge: 20,
            userBirth: '2000/12/22',
            userPhone: 13544231240,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
        {
            userId: 202000041,
            userName: '意外惊喜',
            userSex: '男',
            userAge: 20,
            userBirth: '2000/12/21',
            userPhone: 13544231241,
            userAddress: '湖北省/武汉市',
            userDescript: '这里是默认描述，暂时什么都没有',
            delete: false,
        },
    ];

    // mysql数据库
    function mysql(config) {
        // 导入MySQL包
        let mysql = require('mysql');

        // 创建连接
        let db = mysql.createConnection({
            host: config.host,
            user: config.user,
            password: config.password,
        });
        db.connect(
            err => {
                if (err) throw err;
                console.log('连接成功');
            }
        );

        // 创建数据库
        db.query(`Create Database If Not Exists ${config.db} Character Set UTF8`, (err, result) => {
            if (err) throw err;
            console.log('数据库创建成功');
        });

        // 创建表
        db.query(`Create Table If Not Exists ${config.db}.${config.table}(
        userId int Primary key Auto_Increment,
        userName VARCHAR(255),
        userSex VARCHAR(255),
        userAge int,
        userBirth VARCHAR(255),
        userPhone VARCHAR(255),
        userAddress VARCHAR(255),
        userDescript VARCHAR(255),
        userDelete VARCHAR(255)
        )ENGINE=InnoDB AUTO_INCREMENT 202000001 DEFAULT CHARSET=utf8;`,
            (err, result) => {
                if (err) throw err;
                console.log('表创建成功');
            });

        // 添加数据
        for (let i = 0; i < Data.length; i++) {
            let arr = [
                Data[i]["userName"],
                Data[i]["userSex"],
                Data[i]["userAge"],
                Data[i]["userBirth"],
                Data[i]["userPhone"],
                Data[i]["userAddress"],
                Data[i]["userDescript"],
                Data[i]["delete"]
            ];
            db.query(`insert into ${config.db}.${config.table} values (null,?,?,?,?,?,?,?,?)`, arr, (err, result) => {
                if (err) throw err;
                if (i == Data.length - 1) console.log('数据添加成功');
            });
        }
    }

    // mongodb数据库
    function mongodb(config) {
        // 导入mongodb模板
        let MongoClient = require('mongodb').MongoClient;
        // 连接地址
        let url = `${config.url}\\${config.db}`;

        // 创建数据库和集合并插入数据
        MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
            if (err) throw err;
            console.log("数据库已创建!");
            var dbo = db.db(config.db);
            dbo.collection(config.table).insertMany(Data, function (err, res) {
                if (err) throw err;
                console.log("插入的文档数量为: " + res.insertedCount);
                db.close();
            });
        });
    }

    module.exports = {
        mysql,
        mongodb
    }
})();