//引入工具文件
var db = require("./app_data.js");

//数据插入到mysql数据库格式
db.mysql({
    host : 'localhost',  // 连接地址
    user : 'root',       // 连接用户名
    password : '123',    // 连接密码 
    db : 'user_db',        // 创建的数据库名称
    table: 'user_table',    // 创建的表的名称
});