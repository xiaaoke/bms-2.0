$(function () {
    // 设置用户名
    $("#name").text(window.location.search.split("=")[1]);

    // 退出
    $('#out').click(function (e) {
        $.get('/out', data => {
            if (data.status == 200)
                (location.href = "./login.html");
        });
    });

    // 当前页面页数  总页数  页面容量
    let pageIndex = 1;
    let pageEnd = 0;
    const pageSize = 10;
    // 获取输入的检索条件
    let query = $("#J_search").val();
    // 年月日渲染
    timeFn();
    // 城市三级联动渲染
    cityFn();
    // 初始数据渲染
    dataFn();

    // 姓名失去焦点事件
    $('.model-name input').blur(function () {
        if (!/^[\u4e00-\u9fa5]{2,4}$/.test($(this).val())) {
            $(this).next().text('2~4个汉字！');
        } else {
            $(this).next().text('');
        }
    });

    // 手机号失去焦点事件
    $('.model-phone input').blur(function () {
        if (!/^1[34578]\d{9}$/.test($(this).val())) {
            $(this).next().text('请输入正确的手机号！');
        } else {
            $(this).next().text('');
        }
    });

    // 判断非空
    function htmlRegExp() {
        if (!$('.model-name input').val() || !$('.model-age input').val() || !$('.model-phone input').val()) {
            return false;
        }
        if ($('.model-name span').text() || $('.model-phone span').text()) {
            return false;
        }
        return true;
    }

    // 年月日方法
    function timeFn() {
        // 年
        yearFn();

        // 月
        $("#J_year").change(monthFn);

        // 日
        $("#J_month").change(dayFn);
    }

    // 设置年份
    function yearFn() {
        let frag = document.createDocumentFragment();
        frag.appendChild(new Option('--请选择--'));
        for (let i = new Date().getFullYear(); i > 1900; i--) {
            frag.appendChild(new Option(i));
        }
        $("#J_year").html(frag);
    }

    // 设置月份
    function monthFn() {
        let frag = document.createDocumentFragment();
        frag.appendChild(new Option('--请选择--'));
        $(".model-age input").val(new Date().getFullYear() - $(this).find("option:selected").text());
        for (let i = 1; i <= 12; i++) {
            if (i < 10)
                frag.appendChild(new Option("0" + i));
            else
                frag.appendChild(new Option(i));
        }
        $("#J_month").html(frag);
    }

    // 设置天份
    function dayFn() {
        let frag = document.createDocumentFragment();
        frag.appendChild(new Option('--请选择--'));
        let y = +$("#J_year").val();
        let m = +$("#J_month").val();
        let max = 0;
        switch (m) {
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                max = 31;
                break;
            case 4: case 6: case 9: case 11:
                max = 30;
                break;
            case 2:
                if ((y % 4 == 0 && y % 100 == 0) || y % 100 == 0)
                    max = 29;
                else
                    max = 28;
            default:
                break;
        }
        for (let i = 1; i <= max; i++) {
            if (i < 10)
                frag.appendChild(new Option("0" + i));
            else
                frag.appendChild(new Option(i));
        }
        $("#J_day").html(frag);
    }

    // 城市三级联动
    function cityFn() {
        // 省
        cityPublic($("#J_province"), data);
        // 市
        $("#J_province").change(townFn);

        // 区
        $("#J_town").change(disrictFn);
    }

    // 设置市
    function townFn() {
        let frag = document.createDocumentFragment();
        let r = $('#J_province').prop('selectedIndex') - 1;
        let pArr = 0;
        r >= 0 && (pArr = data[r]['c']);
        $("#J_town").html("");
        $("#J_disrict").html("");
        $("#J_disrict").append(new Option('--请选择--', -1));
        cityPublic($("#J_town"), pArr);
    }

    // 设置区
    function disrictFn() {
        let frag = document.createDocumentFragment();
        let r = $('#J_province').prop('selectedIndex') - 1;
        let c = $('#J_town').prop('selectedIndex') - 1;
        let dArr = 0;
        if (r >= 0 && c >= 0)
            (dArr = data[r]['c'][c]['c']);
        $("#J_disrict").html("");
        cityPublic($("#J_disrict"), dArr);
    }

    // 三级联动公共部分
    function cityPublic(jquery, arr) {
        let frag = document.createDocumentFragment();
        frag.appendChild(new Option('--请选择--', -1));
        for (let i = 0; i < arr.length; i++) {
            frag.appendChild(new Option(arr[i]['n'], arr[i]['i']));
        }
        jquery.append(frag);
    }

    // 清空模态框值
    function reset() {
        // 姓名
        $(".model-name input").val("");
        // 性别
        $("#nan").prop("checked", "true");
        $("#nv").prop("checked", false);
        // 年龄
        $(".model-age input").val("");
        // 联系方式
        $(".model-phone input").val("");
        // 描述
        $(".model-descript textarea").val("");

        // 生日
        yearFn();
        $("#J_month").html(new Option('--请选择--', -1));
        $("#J_day").html(new Option('--请选择--', -1));

        // 地址
        $('#J_province').html("");
        $("#J_town").html(new Option('--请选择--', -1));
        $("#J_disrict").html(new Option('--请选择--', -1));
        cityFn();
    }

    // 渲染页面数据
    function dataFn() {
        $.ajax({
            type: 'get',
            url: '/list',
            data: {
                pageIndex,
                pageSize,
                query
            },
            success: (data) => {
                if (data.status == 400) {
                    $('.box-title').css('display', 'none');
                    console.log(data.msg)
                } else {
                    $('.box-title').css('display', 'block');
                    // 设置数据到页面
                    $('#J_Data').html(template("tpl", data));

                    // 设置页面总数
                    pageEnd = data.totalPages;

                    // 设置页码
                    $("#J_page").text(pageIndex + "/" + data.totalPages);
                }
            },
            dataType: 'json'
        });
    }

    // 上一页
    $("#J_prev").click(function () {
        if (pageIndex > 1)
            pageIndex--;
        dataFn();
    });

    // 下一页
    $("#J_next").click(function () {
        if (pageIndex < pageEnd)
            pageIndex++;
        dataFn();
    });

    // 条件查询
    $('#J_search').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            query = Number($(this).val());
            dataFn();
            $(this).val("")
        }
    });

    // 绑定修改数据事件
    $('#J_Data').on('click', '.box-list', function () {
        $("#J_model").css('display', 'block');
        $("#J_title").html('修改数据');
        $("#J_model").data("IF", false);

        $(".model-id input").val($(this).find(".list-id").text());// id
        $(".model-id input").attr('data-id', $(this).find(".list-id").data('id'));// id-data

        $(".model-name input").val($(this).find(".list-name").text());// name
        if ($(this).find(".list-sex").text() === '男')// sex
            $("#nan").prop("checked", "true");
        else
            $("#nv").prop("checked", "true");

        $(".model-phone input").val($(this).find(".list-phone").text());// phone
        $(".model-descript textarea").val($(this).find(".list-descript").text());// descript

        // 生日
        let arr = $(this).find(".list-birth").text().split("/");
        $(`#J_year option:contains(${arr[0]})`).attr('selected', true);
        monthFn();
        $(`#J_month option:contains(${arr[1]})`).attr('selected', true);
        dayFn();
        $(`#J_day option:contains(${arr[2]})`).attr('selected', true);

        // 地址
        let arrar = $(this).find(".list-address").text().split("/");
        $(`#J_province option:contains(${arrar[0]})`).attr('selected', true);
        townFn();
        $(`#J_town option:contains(${arrar[1]})`).attr('selected', true);
        disrictFn();
        $(`#J_disrict option:contains(${arrar[2]})`).attr('selected', true);

        $(".model-age input").val($(this).find(".list-age").text());// age
        return false;
    });

    // 绑定删除点击事件
    $('#J_Data').on('click', '.delete-span', function () {
        if (confirm("是否删除？")) {
            let id = $(this).parent().siblings().eq(0).data("id");

            // 发起请求
            $.get('/delete', { id }, data => {
                // 渲染页面数据
                data.msg === 'OK' && dataFn();

            });
        }
        return false;
    });

    // 打开添加页面
    $("#J_add").click(function () {
        $.ajax({
            type: 'get',
            url: '/list',
            success: data => {
                $(".model-id input").val(data.length);
            }
        });
        reset();
        $("#J_model").css('display', 'block');
        $("#J_title").html('添加数据');
        $("#J_model").data("IF", true);
    });

    // 关闭模态框
    $("#J_cancel").click(function () {
        $("#J_model").css('display', 'none');
        // 关闭后清空数据
        $(".model-id input").val("");
        reset();
    });

    // 添加/修改数据到数据库
    $(".model-btn a").eq(1).click(function () {
        let userBirth = '';
        if ($("#J_year option:selected").text() == '--请选择--') {
            userBirth = "";
        } else if ($("#J_month option:selected").text() == '--请选择--') {
            userBirth = `${$("#J_year option:selected").text()}`;
        } else if ($("#J_day option:selected").text() == '--请选择--') {
            userBirth = `${$("#J_year option:selected").text()}/${$("#J_month option:selected").text()}`;
        } else {
            userBirth = `${$("#J_year option:selected").text()}/${$("#J_month option:selected").text()}/${$("#J_day option:selected").text()}`;
        }

        let userAddress = "";
        if ($("#J_province option:selected").text() == '--请选择--') {
            userAddress = "";
        } else if ($("#J_town option:selected").text() == '--请选择--') {
            userAddress = `${$("#J_province option:selected").text()}`;
        } else if ($("#J_disrict option:selected").text() == '--请选择--') {
            userAddress = `${$("#J_province option:selected").text()}/${$("#J_town option:selected").text()}`;
        } else {
            userAddress = `${$("#J_province option:selected").text()}/${$("#J_town option:selected").text()}/${$("#J_disrict option:selected").text()}`;
        }

        let datas = {
            id: $(".model-id input").data('id'),
            userId: $(".model-id input").val(),
            userName: $(".model-name input").val(),
            userSex: $(":radio[name=sex]:checked").val(),
            userAge: $(".model-age input").val(),
            userBirth: userBirth,
            userPhone: $(".model-phone input").val(),
            userDescript: $(".model-descript textarea").val(),
            userAddress: userAddress
        }

        // 非空验证
        if (htmlRegExp()) {
            if ($("#J_model").data("IF")) {
                // 添加数据到数据库
                $.post("/add", datas, data => {
                    $("#J_model").css('display', 'none');
                    // 渲染页面数据
                    data.msg === 'OK' && dataFn();
                }, 'json');
            } else {
                // 修改数据到数据库
                $.post("/update", datas, data => {
                    $("#J_model").css('display', 'none');
                    // 渲染页面数据
                    data.msg === 'OK' && dataFn();
                }, 'json');
            }

            // 关闭后清空数据
            $(".model-id input").val("");
            reset();
        } else {
            alert('您的信息没有填写成功');
        }
    });

    // 重置
    $(".model-btn a").eq(0).click(reset);
});