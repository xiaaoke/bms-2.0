/**
 * 1. 访问静态页面 express静态资源托管
 * 2. 增删改查接口 请求方式 get post
 * 3. 操作数据库  mongodb 
 * 4. 渲染前端页面 Ajax from
 * 
 * 操作步骤
 * 1. npm init -y
 * 2. npm i express,mongodb,body-parser,svg-captcha,cookie-session
 * 3. 静态资源托管
 * 4. 编写接口（路由）
 * 5. postman 测试
 * 6. 前端页面渲染
 */

// 导入模块
const express = require('express');
const bodyParser = require("body-parser");
const svgCaptcha = require('svg-captcha');
const cookieSession = require('cookie-session')
const dbTool = require('./libs/dbTool')

// 创建服务器
const app = express();

// 静态资源托管
app.use(express.static('views'));

// 注册中间件
// bodyParser
app.use(bodyParser.urlencoded({ extended: false }));

// cookieSession
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2'],

    // 失效时间
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));

// 判断登录
app.use((req, res, next) => {
    if (req.url.indexOf("list") !== -1) {
        // 说明这个接口必须是在登录状态下访问
        if (req.session.userName) {
            // 已登录   继续往下
            next();
        } else {
            res.send({
                msg: "请先登录",
                status: 400
            });
        }
    } else {
        next();
    }
});

// 写路由
// 1.查询数据
app.get("/list", (req, res) => {
    // 查询内容   页码   页容量
    let query = req.query.query;
    let pageIndex = req.query.pageIndex;
    let pageSize = req.query.pageSize;

    // 数据库查询数据
    dbTool.find('user_data', {}, results => {
        // 总长度
        const length = results.length + 202000001;

        // 根据query检索符合要求的数据
        const temArr = results.filter((value, index) => {
            return (value.userId.toString().indexOf(query) !== -1 || value.userName.indexOf(query) !== -1 || value.userPhone.toString().indexOf(query) !== -1);
        });

        let list = [];

        // 计算起始索引
        const startIndex = (pageIndex - 1) * pageSize;
        // 计算结束索引
        const endIndex = pageIndex * pageSize - 1;
        // 获取当前页码的数据
        for (let i = startIndex; i <= endIndex; i++) {
            if (temArr[i]) {
                list.push(temArr[i]);
            }
        }

        // 获取总页数
        const totalPages = Math.ceil(temArr.length / pageSize);
        // 把数据和总页数,数据最末id返回
        res.send({
            list,
            totalPages,
            length
        });
    });
});

// 2.添加数据
app.post("/add", (req, res) => {
    // 获取post请求参数
    // console.log(req.body);

    dbTool.insertOne('user_data', req.body, () => {
        res.send({
            msg: 'OK',
            status: 100
        });
    });
});

// 3.删除单个数据
app.get("/delete", (req, res) => {
    // 获取get请求参数
    const id = req.query.id;

    dbTool.deleteOne('user_data', { _id: dbTool.ObjectId(id) }, () => {
        res.send({
            msg: 'OK',
            status: 100
        });
    });
});

// 4.修改数据
app.post("/update", (req, res) => {
    // 获取post请求参数
    const id = req.body.id;

    dbTool.updateOne('user_data', { _id: dbTool.ObjectId(id) }, req.body, () => {
        res.send({
            msg: 'OK',
            status: 100
        });
    });
});

// 5.用户注册
app.post("/register", (req, res) => {
    dbTool.find("user_table", { userName: req.body.userName }, result => {
        if (result.length == 0) {
            dbTool.insertOne("user_table", req.body, result => {
                res.send({
                    msg: "用户注册成功，点击确认跳转到登录界面",
                    status: 400
                });
            });
        } else {
            res.send({
                msg: "你输入的账号已被占用, 请换其他的",
                status: 401
            });
        }
    });
});

// 6.验证码
app.get('/captcha', (req, res) => {
    var captcha = svgCaptcha.create();
    req.session.captcha = captcha.text;

    res.type('svg');
    res.status(200).send(captcha.data);
});

// 7.用户登录
app.post("/login", (req, res) => {
    // 获取前端传递过来的数据
    const userName = req.body.username;
    const userPassword = req.body.password;
    const vcode = req.body.vcode.toString().toUpperCase();

    // 验证码验证
    if (req.session.captcha.toString().toUpperCase() === vcode) {

        // 用户名和密码的验证
        dbTool.find("user_table", {
            userName,
            userPassword
        }, result => {
            if (result.length === 0) {
                res.send({
                    msg: "您的用户名或者密码错误",
                    status: 400
                })
            } else {
                req.session.userName = userName;
                res.send({
                    msg: "登陆成功",
                    status: 200,
                    userName
                })
            }
        });
    } else {
        res.send("NO");
    }

});

// 8.弹出接口
app.get("/out", (req, res) => {
    // 清除session
    req.session = null;

    res.send({
        msg: "登出成功",
        status: 200
    })
})


// 启动服务器
app.listen(4399);